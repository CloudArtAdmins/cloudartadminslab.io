---
layout: page
title: Resources 
permalink: /resources/
---

Pages for resource based posts.

#### With a Post
<ul>
  {% for post in site.categories.resources %}
    {% if post.url %}
      <li><a href="{{ post.url }}">{{ post.title }}</a></li>
    {% elsif post.url == 0 %}
      <p>There are no resources yet!</p>
    {% endif %}
  {% endfor %}
</ul>

