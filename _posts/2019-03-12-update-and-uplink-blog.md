---
layout: post
title: "12 Mar. 2019: Update & Uplink"
date: 2019-03-12T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  A successful reproduction fo the uplink node.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
 - FCC
---
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[FCC]: https://www.FCC.gov 'Federal Communications Commission'
[FCCCP]: https://www.fcc.gov/licensing-databases/licensing 'FCC Compliant Provider license'
<<<<<<< HEAD
[CDdocuments]: /assets/other/code_day_2019.md 'Documentation from Code Day'
=======
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c

## Uplink Node Reproduction
While working with [our lead developer][Lead], we successfully created a temporary mesh uplink on one of the oldest devices we have.  
Meaning: rebooting the device will reset the configurations.

We have left the not greatly informative notes that may or may not be factors in proper setup in the root of [Project Mystery Castle][PMC].  

I have assigned someone to develop the script for temporary clients, we can now get to work on setting up an uplink that has temporary configurations.  
So, what is currently being developed?  
* Bash script to automate temporary client device setup.
<<<<<<< HEAD
  * [Like during code day][CDdocuments].
  * Currently being created by someone else.
* Ansible playbook to automate permanent gateway.
  * Currently being created by someone else.
* Bash script to automate temporary gateway & uplink device (these can be the same device).
=======
  * [Like during code day].
  * Currently being created by someone else.
* Ansible playbook to automate permanent gateway.
  * Currently being created by someone else.
* Bash script to automate temporary gateway & uplink device. (These can be the same device)
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
  * I will likely be the one creating these.
* Ansible playbooks for permanently configuring the uplink devices.
  * I will likely be the one creating this.

Will bash scripts or Ansible playbooks come first?  
We will see.  

## FCC Requirements Acknowledgement
My objective while working with [Project Phoenix][ProjectPhoenix] was originally to develop a method of updating the mesh devices.
It will likely change to just automation development.  

Reasons:  
[Project Phoenix][ProjectPhoenix] wants to support the community and supply hardware.  
[Project Phoenix][ProjectPhoenix] does not want to manage everyone's hardware.  
[Project Phoenix][ProjectPhoenix] is meant to help educate the community and provide internet accessibility.  
[Project Phoenix][ProjectPhoenix] is not supposed to configure devices and leave nobody knowing about how.  
The [FCC][FCC] has requirements and regulations to follow if an entity distributes custom firmware.  
If [Project Phoenix][ProjectPhoenix] wants to update hardware as we update our scripts, then it needs to be an [FCC compliant provider (licenses)][FCCCP].  

Thus, [Project Phoenix][ProjectPhoenix] does not want to be an FCC compliant provider.  

## Temporary Verses Permanent Configurations
There is one last factor to consider at this point; what devices do we want to have a permanent configuration and what devices should only have a temporary configuration?

Permanent devices will likely be similar to the super-nodes.  
Temporary devices might be users who want to temporarily join the network.  
The [Project Phoenix][ProjectPhoenix] setup will be talked about at a later date, but these are things to consider when setting it up for the community.

