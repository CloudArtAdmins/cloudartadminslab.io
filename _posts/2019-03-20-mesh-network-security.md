---
layout: post
title: "Security of Mesh Networks"
date: 2019-03-20T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  One of the main questions for mesh networks is "how do you do security?".
  It is an internal network, and everything moving using BATMAN is plain-text.
  "This sounds horribly insecure! Why would you use it?!"
categories: project
thumbnail: fa-ansible
tags:
 - security
 - mesh
 - networking
 - topology
---

# A Modern Look

Mesh networks were one of the original type of networking for devices.  
Computers were directly wired together to communicate with one another.
As time went on, more topologies were created to reduce resource costs for connecting devices together.
Eventually, we come to today, where topologies are taught with the idea of wires in mind, still... (most of the time).
This isn't necessarily wrong to do, as the world is currently connected together through wires, but it often disregards the capabilities of wireless networking.

## Comparing Topologies

<<<<<<< HEAD
![Picture of network topologies (To view, allow images from PrivateSeabass.Gitlab.io)](/assets/networkDiagrams/Topologies.png)

#### Star

![Star topology picture (To view, allow images from PrivateSeabass.Gitlab.io)](/assets/networkDiagrams/starTopologies.png)  
=======
![Picture of network topologies (To view, allow images from PrivateSeaBass.Gitlab.io)](/assets/networkDiagrams/Topologies.png)

#### Star

![Star topology picture (To view, allow images from PrivateSeaBass.Gitlab.io)](/assets/networkDiagrams/starTopologies.png)  
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c

The star topology is based on all devices connecting to one device, which connects them to each other.
Data goes to the switch/hub, and is forwarded to the destined device.
For a regular house hold, this is the main topology.

<<<<<<< HEAD
![Star topology with connected by wires picture (To view, allow images from PrivateSeabass.Gitlab.io)](/assets/networkDiagrams/looseStar.png)  
=======
![Star topology with connected by wires picture (To view, allow images from PrivateSeaBass.Gitlab.io)](/assets/networkDiagrams/looseStar.png)  
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
This can be done with a hub/switch and requires each computer to have one Ethernet cable.  
This can also be done with a wireless switch and a WiFi card, instead of a cable.

Minimum devices to go through to get to proper device (hops): 1

#### Mesh
<<<<<<< HEAD
![Mesh topology picture (To view, allow images from PrivateSeabass.Gitlab.io)](/assets/networkDiagrams/NetworkTopologies.jpg)  
=======
![Mesh topology picture (To view, allow images from PrivateSeaBass.Gitlab.io)](/assets/networkDiagrams/NetworkTopologies.jpg)  
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c

The mesh topology is based on devices connecting each other together, rather than connecting from a central device (central device is most common, today).

There are two methods of describing this part...

##### When only dealing with mesh network compatible devices:
Devices will send a message to a device.
If the receiving device is the destination, it stops there.
If the receiving device is not the destination, it will be sent to another device closer to the destination.

##### Considering devices without mesh networking protocols:
This often requires something, such as a super-node, available for users to connect. 
This super node grants users access to the mesh network.
From there, the the outer internet is optional.  
Now, I can imagine someone saying "Seabass, you just left the LAN (Local Area Network), why didn't you talk about outside the LAN for the star topology?!"  
Well, hypothetical reader, this is because those who are on the mesh network, with a mesh compatible device, that previously mentioned "not LAN area" _IS_ the local area network for mesh topologies.

If the world used mesh networks, there would be no separation of local area network and wide area networks. 
This isn't to say that LANs would be inexistent, just that for general internet connection use by Joe-Shmoe, a local area network is unnecessary.

This is where the term "decentralized" fits. 
All devices that existing on a mesh network is not that likely to happen any time soon. 
What currently exists is a method of using something such as a router to broadcast a WiFi that allow devices without mesh protocols to connect to the mesh through it, that is it.

Wonder something more or question something mentioned about either mesh or star? [send me an email](/about.md).

# Possible Solutions
In reality, Mesh is not that, by perspective of what needs to be secured over the internet. 
Will there be different vectors of attack? Yes. 
Does this mean that one is inherently more or less secure (discounting availability)? Not really.  
Meaning: the methods people use to obtain security and privacy are the same as the current methods, at least as long as the two methods coexist (we can expect it to be a while).  

* HTTPS
Like for any other use, have an encrypted connection to wherever you go.   
It may be imperfect, but it is the easiest first line of defense for many users.
* VPNs
Open-mesh and non-layer 2 based encryption usage

These are two simple solutions that should be used in general anyways.  
So... Is there really a problem with mesh network security that isn't currently there, to begin with?  
Is it really any different?

