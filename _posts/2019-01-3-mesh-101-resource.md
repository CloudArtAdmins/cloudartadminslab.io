---
layout: post
title: "Mesh Networking 101 by SilentSysAdmin"
date: 2019-05-21T12:00:00-7:00
author: Will McDonald
summary: > 
  A quick guide to follow when beginning your jourey down the mesh-networking rabbit hole

categories: resources
thumbnail: fa-resources
Tags: 
  - mesh-networking 
  - quick-start 
  - reference
  - mesh
---
To see more content by the SilentSysAdmin, got to [SilentSysAdmin.com](SilentSysAdmin.com)

If you're interested in contributing to Project Phoenix, or starting your own
community mesh network, this post should help get you on your way. These links
were compiled from my personal research on the topic.

### Getting Started
- - - 

If you're not aware of what a mesh network is, take a look at these links to
gain a basic understanding of the typology. 

  - [Understanding Mesh Networking, Part I](https://inthemesh.com/archive/understanding-mesh-networking-part-i/)
  - [Mesh Networking Wikipedia Article](https://en.wikipedia.org/wiki/Mesh_networking)
  - [Community Wireless Networking Wikipedia Article](https://en.wikipedia.org/wiki/Wireless_community_network)
  - [Build your own!](https://gitlab.com/projectphx/Project_Mystery-Castle)

### Mesh Networking Protocols
- - -

These are fundamental protocols that should be reviewed before moving forward. I
would recommend checking out B.A.T.M.A.N-ADV which is a fantastic example of
what's currently possible. 

  - [B.A.T.M.A.N-ADV](https://www.open-mesh.org/projects/open-mesh/wiki)
    - [B.A.T.M.A.N-ADV Quick start guide](https://www.open-mesh.org/projects/batman-adv/wiki/Quick-start-guide)
  - [BMX6 - Mesh routing protocol for Linux systems](https://github.com/bmx-routing/bmx6)
  - [Babel loop-avoiding distance-vector routing protocol for wireless mesh networks](https://tools.ietf.org/html/rfc6126)
  - [Open Shortest Path First (OSPF)](https://en.wikipedia.org/wiki/Open_Shortest_Path_First)

### Advanced Resources
- - -

  - [Start your own ISP](https://startyourownisp.com/)
  - [WISPtools eBook](http://wisptools.net/ebook.php)
  - [Building a Wireless Rural Mesh Network eBook](http://wirelessafrica.meraka.org.za/wiki/images/f/fe/Building_a_Rural_Wireless_Mesh_Network_-_A_DIY_Guide_v0.7_65.pdf)

### Hardware
- - - 

I've only noted hardware I've worked with while involved with Project Phoenix.
As we continue to evaluate options, I will be sure to update this post. 

#### Linux compatibility 

Resources to help with determining specific hardware and driver support in the
Linux kernel. Rule of thumb, if you have a semi-modern system with
802.11b,g,n,ac,ax support you will be able to configure a node. We've done so
with a Raspberry Pi Single Board computer. 

  - [Raspberry Pi 3 Model B](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/)
    - [Ralink rt5370](https://wikidevi.com/wiki/Ralink)
      - [802.11b,g,n (2.4G) Wifi Dongle](https://www.ebay.com/itm/150M-USB-WiFi-Wireless-LAN-Adapter-w-Antenna-Raspberry-Pi-ralink-rt5370-MAUS/233220311402?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649)
  - [Wikidevi](https://wikidevi.com/wiki/Main_Page)
  - [Linux Wireless - Kernel.org](https://wireless.wiki.kernel.org/welcome)

##### Ubiquiti

Airmax equipment is projected to be used for P2P links between Level 2 (Carriers) and
Level 1 (Supernodes) nodes.

**NOTE:** Unfortunately, Ubiquiti has locked down the ability to upload custom
firmwares to their Nanostation devices. We've spent some time attempting to
bypass the firmware signature check with no luck. [Feel free to reach out to me
if you know a workaround.](mailto:will@azblockchain.org)

  - Nanostation M Series
    - [Firmware and Documentation Portal](https://www.ui.com/download/)
    - [Nanostation M5](https://store.ui.com/products/nanostation-m5)
    - [Nanostation Loco M5](https://store.ui.com/products/nanolocom5)

##### Mikrotik

  - Hex Series
    - [Hex PoE Lite](https://mikrotik.com/product/RB750UPr2)
    - [Hex PoE](https://mikrotik.com/product/RB960PGS)

### Firmware
- - -

The majority of firmwares that are in use are cross-platform (ARM, MIPS, x86, x86_64, Etc.) 

  - [OpenWRT](https://openwrt.org/)
      - [Wikipedia Article](https://en.wikipedia.org/wiki/Openwrt)
    - [Hardware database](https://openwrt.org/toh/start)
    - [Documentation Portal](https://openwrt.org/docs/start)
  - [Quick Mesh Protocol](https://www.qmp.cat/Home)
    - **NOTE:** Quick Mesh Protocol's Last Major release was in Jan 2018. Not likely to be used in Project Phoenix at this time.
  - [Alpine Linux](https://www.alpinelinux.org/)
    - [Wikipedia Article](https://en.wikipedia.org/wiki/Alpine_Linux)
    - [Downloads](https://alpinelinux.org/downloads/)
    - [Documentation Portal](https://wiki.alpinelinux.org/wiki/Main_Page)

  - [MikroTik RouterOS](https://wiki.mikrotik.com/wiki/Manual:RouterOS_features#QoS)
    - [Documentation Portal](https://wiki.mikrotik.com/wiki/Manual:TOC)

### Legal
- - -

  - [EFF Openwireless movement](https://openwireless.org/)

### Other projects
- - -

To avoid re-inventing the wheel, these projects provide fantastic documentation
that has come in handy when designing the network. 

  - [Guifi](http://guifi.net/en)
  - [Sudomesh](https://sudoroom.org/wiki/Mesh)
    - [PeoplesOpen.net](https://peoplesopen.net/)
  - [NYCMesh](https://www.nycmesh.net/)
  - [wlanslovenija](https://wlan-si.net/)
  - [Librerouter](https://librerouter.org/)

### Media
- - - 

  - [In the MESH: Dedicated to covering the decenteralization movement](https://inthemesh.com/)
