---
layout: page
title: Projects
permalink: /project/
---

Current or past projects.

#### Projects with a post
<ul>
  {% for post in site.categories.project %}
    {% if post.url %}
      <li><a href="{{ post.url }}">{{ post.title }}</a></li>
    {% elsif post.url == 0 %}
      <p>There are no projects yet!</p>
    {% endif %}
  {% endfor %}
</ul>

#### Projects without a post

* [Easy Start Linux](https://gitlab.com/PrivateSeabass/easy-start-linux)
* [Project Phoenix](https://gitlab.com/projectphx)
* [EGX Databases](https://gitlab.com/egx)
* "Cyber Hygiene" (not currently public)

