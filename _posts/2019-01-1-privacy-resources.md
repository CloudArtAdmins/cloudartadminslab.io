---
layout: post
title: "Privacy Resources"
date: 2019-02-1T13:13:43-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Database of links to content on privacy.
  Ranges from applications, to practices, to software alternatives, and probably more.
  I will be attempting to keep this updated with time as I find more resources.

categories: resources 
thumbnail: fa-resources
tags:
  - privacy
  - database
  - links
  - security
  - guidance
---

### Beginner Guides to Privacy
* [Thirty Day Security Challenge](https://operational-security.com/thirty-day-security-challenge/)
  * Just getting into security or privacy habits? Start here.
  * Good slow instruction time span
  * Simple aspects, if you are more advanced, you have likely already done these.
* [8-DAY DETOX](https://datadetox.myshadow.org/en/detox)
  * 8 days of instructions for simpler privacy needs.
  * Has a small overview
  * Interactive
  * Simple explanations for taking these actions

### Links to Information
* [Privacy Tools](Privacytools.io)
  * Keeps up to date pretty well
  * Large amount of suggested software, alternatives, and general privacy practice suggestions
* [That One Privacy Site](thatoneprivacysite.net)
  * Very good resource for finding a VPN or E-mail provider
  * Has both detailed and simple descriptions
  * Don't know how to pick? There is a description for "how", too
* [Intel Techniques](inteltechniques.com)
  * Privacy and OSINT focused forum
  * A resource for more serious situations
  * Training for OSINT (Used to find where in life privacy needs reinforcements)
* [Operational Security](https://operational-security.com/)
  * Blog for privacy information
  * Software security focused
* [EFF](eff.org)
  * Provides many articles on current privacy information
  * Non-profit dedicated to consumer privacy rights
* [Privacy Rights Clearinghouse](https://www.privacyrights.org/)
  * Go here to complain about an organization's privacy experiences
  * Blogs about privacy
  * Houses reports on Data Breaches
* [Retro64xyz](retro64.xyz)
  * Blog for security and legal implications of privacy tools
  * Sometimes provides video presentations for topics
  * Much more in-depth explanations that most.
* [Firefox Privacy – The Complete How-To Guide](https://restoreprivacy.com/firefox-privacy/)
  * Most comprehensive Firefox guide to privacy that I have found.
  * Meant for Firefox specific
  * explains whys for everything it instructs
* VPN Kill Switch firewall setup links.
  * [Prevent Any Traffic from VPN Hosts from Egressing the WAN](https://www.infotechwerx.com/blog/Prevent-Any-Traffic-VPN-Hosts-Egressing-WAN)
  * [How to: Proper partial network VPN with "kill switch" (reddit)](https://www.reddit.com/r/PFSENSE/comments/6edsav/how_to_proper_partial_network_vpn_with_kill_switch/)
  * These should both work, despite the VPN one uses.
  * Important for ensuring protection from leaks.
  * A bit more difficult a task, but far easier given these instructions.

### Links to Privacy Focused Articles 
* [Google Analytics Alternatives](https://www.fastcompany.com/90300072/its-time-to-ditch-google-analytics)
  * Includes web dev google alternatives.
  * Displays the beliefs of privacy focused website owners.
* [Bye, Bye, Google](https://defn.io/2019/02/04/bye-bye-google/)
  * Site full of alternatives for people who's lives are entrenched in Google products.
  * Includes web dev google alternatives.
* [Git "Font Degooglifier"](https://git.occrp.org/libre/fonts-degooglifier)
  * Project to remove google fonts
  * Downloads them and stores it on the page
  * Doesn't pull from Google for every user.
* [Privacy Is Just the First Step, the Goal Is Data Ownership](https://thetoolsweneed.com/privacy-is-just-the-first-step-the-goal-is-data-ownership/)
  * Understands that privacy for privacy's sake is unreasonable
  * Gives examples for the non-technical
  * Describes possible solutions to issues

