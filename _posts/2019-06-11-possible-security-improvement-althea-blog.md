---
layout: post
title: "11 Jun 2019: Possible Security Improvement & Althea Mesh"
date: 2019-06-11T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Talked to Althea mesh recently. We may have found a method to improve security for the users of the mesh.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
 - security
 - VPN
 - academy
 - althea
---
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[PCA]: https://www.phoenixunion.org/coding 'Phoenix Coding Academy'
[AM]: https://althea.org/ 'Althea Mesh'
[WhitePaper]: https://althea.org/whitepaper 'Althea White Paper'

Last week had a lot going on.
[Project Phoenix][ProjectPhoenix] had an internal meeting, a meeting with [Althea Mesh][AM], a meeting with someone from [Phoenix Code Academy][PCA], and may begin including some new volunteers.  

All of these talk about the future of the mesh project, but here are bullets for their main takeaways.

## [Althea Mesh][AM] Meeting
* We were informed that
  * Althea's hardware comes with mandatory encryption between device using VPN tunnels.
  * Althea has done a lot of the research we were going to need to learn on our own. (We will be getting a lot of advice from them)
  * Most other mesh projects apparently have Matrix servers. (Posting in my [Mesh Resources page][Mesh-Resources])
  * They have a [White-Paper][WhitePaper] on their setup, which we will be viewing.
* They let us know that VPN tunnels can be set up by default for each device, and that would allow them to encrypt traffic between each other.
  * This shouldn't be that big an issue, as they are directly communicating with each other
  * When not directly communicating, it prevents others from seeing traffic.
  * It has significant throughput drops for use, but this may be okay for us.
  * If this is not what we would need, we don't need to implement it.
  * I like the idea, it sounds cool. :)
* The leader of Althea did some things to help his mesh projects succeed.
  * The main issue is maintaining funding for the node maintenance and purchase of hardware
  * Their solution was to setup a payment system for using the nodes.
    * Uses cryptocurrency
    * Sales can be based on whatever time span is preferred
    * Can be set up by anyone
    * This can be disabled by making the payment for use 0.
    * Can they branch out to other forms of currency, eventually? I dunno.
    * Please don't let ME know over [THEM][AM] about if you have reservations about using cryptocurrencies.
  * This way, if someone wanted to set up a node, they would be getting paid by users.
  * If they wanted more traffic (better pay) they would thus get better hardware.
  * Ideally, this would make it self sustainable.
* They are otherwise going to help us get in touch with the other mesh projects.

## [Project Phoenix][ProjectPhoenix] Meeting
* We have two people who are wanting to volunteer to support the project
  * I will provide further information later if they are willing to have me put it up.
  * They both do consulting work and have far more experience than [Will][Lead] and I.
  * They have less time, so [Will][Lead] and I will probably be the main developers, still.
  * One may be able to supply 100MB of bandwidth to the mesh, depending on where he is.
  * We need to 
    * have a demo available by August.
    * need to setup our own matrix server.
    * setup an internal communications method.
    * setup some form of official project management.
    * All these things are best practices of other groups; we have found open-source, self-hosted software; and so we have somewhere to start.
  * Remember, if you'd like to volunteer and help us, too, go to the [Project Phoenix Website][ProjectPhoenix].

## [Phoenix Code Academy][PCA]
* The students will be providing support for the [Project Phoenix][ProjectPhoenix] team.
  * Setting up hardware
  * Making compatible programs
    * Ideas were mostly basic programs for emergency situations, first
  * These students will be making "libraries" for users of the Project Phoenix mesh network.
  * Possibly setting up the node in public libraries.
* They may increase the resources in Kolibri, as a result.
* The school may also begin the mesh in their school.
  * Currently hesitant to lend support, due to possible ISP disapproval.
  * This will be a bit of a way in the future, though, so not too worried as it might be a while.


