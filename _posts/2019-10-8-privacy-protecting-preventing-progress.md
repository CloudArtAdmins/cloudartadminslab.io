---
layout: post
title: "Privacy's Protection and Prevention of Progress"
date: 2019-10-08T15:52:43-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Privacy does many things for people.
  It can protect people from problems in the world,
  It prevents researchers from collecting mass amounts of data,
  thus progress verses privacy may become a topic of future contention.
categories: project
thumbnail: fa-privacy
tags:
 - privacy
 - society
 - progress
---

[Alias Name Propagation]: https://privateseabass.gitlab.io/project/2019/05/23/alias-name-propagation/ 'My post on propagating alias names'

Usually my posts inform people about privacy concepts or telling a story that support mesh networking theories.  
This time is different.  
This post will describe a dilemma in privacy that has arisen in my life. 

## Privacy Protects People
My post on [alias names][Alias Name Propagation] describe people with a variety of privacy needs.
One could be Jamie Average, who only needs to protect himself common hackers, robo-callers, and phishers.  
One could be a CEO who needs to protect his personal information from advanced hackers after money or company secrets.  
One could have been Jamie Average, but found herself needing to become more private due to a stalker or stalking ex-lover.  

Online product usage often comes with privacy implications.
People do not appreciate every website we visit knowing every other site we have been on; 
we do not appreciate our data being stored publicly on the internet;
and we do not appreciate companies propagating our information, leaving it vulnerable to abuse.

Perhaps corporations are not gathering our data with malicious intent, but that doesn't mean someone won't do something malicious with it.
Data breaches are a common occurrence, after all.
Today, automated information gathering has spawned data science, a field of study that derives meaning from data.
Data scientists need to group data together somehow, so some companies may attempt to "sanitize" or "anonymize" their user's data.  
Which is appreciated, but may fail.  
When "anonymization/sanitization" of data is done improperly, it may allow malicious people to learn what is need about their target.

Automatically gathered information means so much data is obtained that no company can hire enough employees to individually manage it.
As a result, if one knows how the automated system works, one may be able to obfuscate or even avoid providing unnecessary information.
The problem is that learning to set up something like a catch-all address that forwards emails may be inconvenient.
If people have to learn these processes, privacy protection becomes too inconvenient for most people, thus many stop caring about privacy.
This allows companies to gather massive data stores that are full of information about its users. 

At least the people who put in the effort protect their privacy will be safe when the company's data is exposed.

## Scientific Progress
Scientists need massive amounts of data to perform successful experiments.
Data is used to determine how people are affected by whatever is being tested, and the more information scientists have, the more accurate the results.
This is especially true for data scientists who work in the machine learning facet of Artificial Intelligence (AI) research.
The most accurate machine learning processes have massive data sets that are needed to teach the AI how to accurately identify what it is supposed to.

The United States and China are currently the two main competitors for the best AI research.  
The United States has better data scientists and creates better algorithms and processes.  
China, however, has far larger data sets to work with, due to the established mass surveillance infrastructure that is already present for data gathering.  
For China, information is easy to obtain, abundant, and full of useful information.  
For the United States, information can be obtained, but it is not as abundant and it is easier to obfuscate.  
Thus, the United States creates better systems; China has more data.  

As a result, the question becomes "Which is more important?" to determine which side will obtain AI research superiority, in the long run.  

If the United States wants to obtain and maintain superiority in the study of AI, then the sharing, selling, and obtaining of information by data brokers may be more necessary for success.
Data scientists have created algorithms that require massive amounts of data. 
Do they need all information data brokers may distribute?
Maybe not, but the more information there is the more associations in the data are found.
The more associations between data is found, the more effective the technology becomes.
Thus, data scientists always want all the data possible.

## The Privacy-Progress Dilemma
Privacy advocates will provide reasons personal data should remain secure.  
Data scientists will always work to gather as much accurate information as possible.

As an individual, one needs privacy to protect herself from malicious actors.  
As a country, people need massive data sets to successfully compete with opposing nations.  
Which one should take precedence?

There is a philosophical idea I believe applies well in this situation.  
People who work alone can not work as well as people who work together.  
People who work together may achieve great success through significant effort.  
The person who takes advantage of the combined efforts of others will have the greatest success.  
The more people who take advantage, the less success occurs, until progress stalls.

People may provide their information willingly or unwittingly.
These people's security weakens over time.  
However, privacy minded people will not be a part of large data sets, thus any unwanted sharing of information doesn't effect them. 
Few enough people are privacy minded, so the amount of data they would provide is looked at as insignificant by data scientists and data brokers.
They become targets that require more effort to exploit than everyone else, thus are not worth the time to go after.

I believe people should be able to have private lives.  
From a societal viewpoint, life may become more difficult as a whole, the more people are private.  
A single person with many e-mail addresses is harder to identify because identification may require more information about that person.
Having one email is more convenient, but doesn't provide as much privacy because all data is associate with one account.  
I will trade convenience for privacy benefits.
I'll be one of the few who takes advantage of the privacy benefits in a not very private society.  

So, I'll teach privacy and help others who desire more privacy.
Privacy is useful to me and people around me.  
I can respect either choice, but what will you do?
