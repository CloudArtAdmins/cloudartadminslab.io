---
layout: post
title: "28 Feb. 2019: General Update"
date: 2019-02-28T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Where things currently stand. 
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
 - Tor
---
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Lead]: https://gitlab.com/Will.McDonald 'Will McDonald Gitlab page'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[PB]: https://piratebox.cc/ 'Pirate Box'
[Relay]: https://www.eff.org/torchallenge/what-is-tor.html 'Tor Relays'
[batWrt]: https://www.open-mesh.org/projects/batman-adv/wiki/Batman-adv-openwrt-config 'batman-adv for OpenWrt'
[openWrt]: https://openwrt.org/ 'openwrt site'
[ubik]: https://store.ui.com/products/nanostation-m5 'Ubiquity Hardware'

So, here is where things currently stand, as of yesterday:
* We have [documentation][PMC] with error that prevent internet communication.
* We have [documentation][PMC] that allows for mesh communication
* We have an [automated method][PMC] for setting up and undoing configuration of a Linux device based on Debian.
* We know we can set up [OpenWrt][batWrt] to be compatible with mesh networks and be our up-link. ┬─┬ノ(°–°ノ)
* The device we put [OpenWrt][openWrt] on can't be connected to, even through cables. (╯°□°)╯︵┻━┻
* We are working with [Ubiquity hardware][ubik] for those.
* I have someone working on setting up the Mesh Gateway devices.

Beyond this, I was talking to someone yesterday about mesh. 
His extent of knowledge is from the [Pirate Box][PB], which uses [mesh protocols][BATMAN]. 
The pirate box was not meant to go on the internet, though.

However, he also mentioned that one could improve the user's privacy by sending the data to a [Tor relay][Relay].
As I hear, it is pretty easy. 
Set the DNS to one of the relays, set the IP-tables to function with the relays.
Problem being: what if those IPs go down or move? 
Perhaps we will have good reason for a periodic checker for that. 
We would need to check how much this hinders internet access before deciding to do it, though (or let people choose ╮(╯\_╰)╭ ) .


