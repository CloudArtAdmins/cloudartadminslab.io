---
layout: post
title: "8 Mar. 2019: Friends with The-Man"
date: 2019-03-8T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Some people have talked to government... people.
  Here are some thoughts that had been provided for them.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
 - government
---
[BATMAN]: https://www.open-mesh.org/projects/batman-adv/wiki 'B.A.T.M.A.N. Advanced'
[PMC]: https://gitlab.com/projectphx/Project_Mystery-Castle 'Project Mystery Castle'
[Mesh-Resources]: https://privateseabass.gitlab.io/resources/2019/02/03/decentralized-mesh-information/ 'Mesh Resources'
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'

A friend of mine knows government workers.
While my friend was talking to the government guys, he mentioned ways the government could benefit from a mesh network.

### Why?
The emergency number 911 is a VOIP number.
This means the phone number uses the internet to deliver calls, rather than cellular.

##### Concept:
If the country is using a mesh network, 911 should always function, since it functions by using the internet.
Problems from the Internet's current system of communication would be prevented, maintaining citizen's ability to call 911.  
[This](https://www.nbcnews.com/news/us-news/fcc-launches-probe-centurylink-wake-nationwide-911-outage-n952681)[ won't ](https://www.cnet.com/news/centurylink-outage-that-hit-911-service-spurs-fcc-investigation/)[happen](https://www.lcsun-news.com/story/news/2018/12/27/centurylink-outage-impacts-city-las-cruces/2426579002/).
These are three articles detailing when citizens lost the ability to contact 911, due to the current Internet system failing.

##### How?
If we build a standard mesh networking repeater, it would increase internet access and coverage at a faster rate.
Cars could be built with this repeater.
Car companies wouldn't require any more work if they wanted to connect cars to the internet.  
These devices are wireless and don't require much power (supposedly wouldn't greatly affect a car's battery life). 
People would have more reliable coverage (ie, across the street or down a busy highway), due to mesh devices.  
All these devices maintain the reliability of emergency VOIP numbers, thus ensuring the safety of civilians and stabilizing their connection to the internet.

Acknowledge: This is a conceptual thing. If there are errors, either submit an issue on the site repository or send me an [email](/about.md), and I will fix it.

##### Possible issues to keep in mind
Wireless communication means that there will be interference from trees and mountains.  
There is also the interference connectivity issues, as there are many devices communicating over the same frequency.
These issues need to be recognized, but will get resolved with time.
