---
layout: post
title: "14 Aug 2019: Quick Update"
date: 2019-08-14T10:00:00-07:00
<<<<<<< HEAD
author: PrivateSeabass
=======
author: PrivateSeaBass
>>>>>>> 75a3a68... parent b941a15573ec84c7cffad8c295cb5f898ff4027c
summary: >
  Preparing for something at the end of this month, and preparing for future events.
categories: mesh-blog
thumbnail: fa-mesh-blog
tags:
 - update
 - project
 - phoenix
 - mesh
 - networking
---
[ProjectPhoenix]: https://azblockchain.org/project-phoenix/ 'Project Phoenix page'
[GWCC]: https://www.gatewaycc.edu/ 'GWCC'
[event]: https://www.eventbrite.com/e/project-phoenix-showcase-and-qa-session-tickets-66149746673 'showcase event link'
[newRepo]: https://gitlab.com/projectphx/project-phoenix-ansible-configs 'Current Ansible Playbooks'
[about]: https://privateseabass.com/about 'about page'

Alright, this one is going to be a short and ad-hoc post.  
I'm kind of tired of debugging at the moment, and decided I'd do something else, right now... writing this!  

My college classes are ending, I'll have a week that I am allowing myself to do whatever, before I get back to doing work.
For me, part of whatever is the mesh project, so I'll talk about what was done then and the event, once the event passes.
For now, we are sending out [invitations to the showcase][event] of our mesh.
We have enough materials and set them up to be prepared for showing off at [Gateway Community College][GWCC].  

We have a three tiered system setup, which maitains our clients (level 3), carriers (level 2), and supernodes (level 1).
There seems to be an unusually common issue of "well, why won't they see/talk to each other?".
Most of the time it is because of a BSSID mismatch, but when you can't get into the second device to inspect it (don't have creds)... 
There might be a way to check, in reality, I need to try and find a way to inspect these packets without being on the network, see if  they actually send the BSSID, too.
Ironically, that is our greatest security concern, and most immediate concern of security people we talk to, so I guess I'll be able to speak to it at some point in the future.

I should also note that these systems we are showcasing are still using IPv4.  
Supposedly we have a system that allows us to use IPv6, but we will see if we go with that.
I would like to, but I'm leaving the final choice to the development lead.

On the brighter side, we should have a rocket.chat and Kolibri server up for people to peruse.  
The rocket.chat app is installed on the computers and Kolibri can be reached from a browser.

There may be someone who wants to get into technical writing to help us.
I can do it to some extent, but I'd gladly take that help. 
Documentation is fine, but finding someone who has the desire to do it will hopefully give support for things that shouldn't really be issues.
The one setting up the documentation for Project Phoenix needs help writing it, currently.  
He has sent us questions to support the documentation.
I intend to answer these questions, but Google being Google, I am having account login issues...
Good security guys. Just means nothing if I can't log in.
We try to avoid using them as much as possible, but document sharing is easiest with it google docs, currently.
If anyone knows of a replacement we could set up, [please let me know][about].

I've setup a [new repository for the Ansible Playbooks][newRepo].
Project Mystery Castle is fine, but is our prototype; what is there currently works as is.  
I am just wanting to setup a repository for all of our playbooks that I can keep up to date in it's own space.
New documentation is comming, but it hasn't been made public, yet.
Still, I'll be adapting the changes into the playbooks.

In September, [Project Phoenix][ProjectPhoenix] will have its fund raiser, and hopefully that will go well.
I'll send something on it closer to the date.
Otherwise, since my college education period of life is ending, let's see how things will go in the future.  
It is time for the beginning of change, once again.
