---
layout: post
title: "What is the best PDF reader for Linux? MuPDF!"
date: 2018-09-17T04:52:43-07:00
author: RetroMe
summary: >
  Trying to find the best PDF reader for Linux? Try MuPDF!
categories: software
thumbnail: fa-microchip
tags:
 - software
 - pdf
 - the-best
 - how to
---

# The Best PDF Reader

I am of the opinion that the Unix method is correct. You need software that
does one thing and does it well. If you need a high quality reader application
then you should look into downloading MuPDF. MuPDF does one thing and it does
it right.  It lets you view PDF, XPS, and E-book documents.

You can find the [mupdf home page here][muhome].

## MuPDF At Work

![mupdfexample][muexample]

## How To Use MuPDF

```
usage: mupdf [options] file.pdf [page]
	-p -	password
	-r -	resolution
	-A -	set anti-aliasing quality in bits (0=off, 8=best)
	-C -	RRGGBB (tint color in hexadecimal syntax)
	-W -	page width for EPUB layout
	-H -	page height for EPUB layout
	-I -	invert colors
	-S -	font size for EPUB layout
	-U -	user style sheet for EPUB layout
	-X	disable document styles for EPUB layout
```

It is as easy as -

```
$ mupdf yourbook.pdf
```

Install is a snap as MuPDF is available in most package managers.

```
$ sudo pacman -S mupdf
```

You cannot go wrong with MuPDF.

[muhome]: https://mupdf.com/ 'The mupdf site'
[muexample]: /../assets/images/inserts/09172018-mupdf/mupdfexample.png
