---
layout: page
title: Presentations
permalink: /presentation/
---

For those of you who wanted the unspoken content of presentations.
**Most recent on top.**

#### Without a Post
* [DC480: Where's the Data?(PDF)](/assets/presentations/powerpoints/PossibleUsageOfYourData.pdf)
* [PLUGS: Your Data & Where It Came From (PDF)](/assets/presentations/powerpoints/YourData&WhereItCameFrom.pdf)
* [PLUGS: Your Data & Where It Came From (Youtube Video)](https://youtu.be/JN2GTl1ggn8)
* [PLUGS: Technically Private (PDF)](/assets/presentations/powerpoints/technicallyPrivate.pdf)
* [PLUGS: Private Mobile (PDF)](/assets/presentations/powerpoints/privateMobile.pdf)
* [Central Cybersecurity Meeting: Hiding From The Internet (PDF)](/assets/presentations/powerpoints/hidingFromTheInternet.pdf)

#### With a Post
<ul>
  {% for post in site.categories.presentation %}
    {% if post.url %}
      <li><a href="{{ post.url }}">{{ post.title }}</a></li>
    {% elsif post.url == 0 %}
      <p>There are no presentations yet!</p>
    {% endif %}
  {% endfor %}
</ul>


